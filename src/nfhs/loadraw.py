import os
import pandas as pd
import yaml

from tqdm import tqdm

DATA_DIR = os.path.join('data', 'IAKR74FL')
datfile = os.path.join(DATA_DIR, 'IAKR74FL.DAT')
dictfile = os.path.join(DATA_DIR, 'datadict.yml')
# pd.read_csv(datfile, sep=r'\s+')
datadict = yaml.load(open(dictfile))
df = [k.split() + v.split('-') for k, v in datadict.items()]
df = pd.DataFrame(df, columns='typ name one start stop'.split())
del df['one']
df = df.replace({'byte': 'str', 'long': 'int'})
df['start'] = df['start'].astype(int)
df['stop'] = df['stop'].astype(int)
typdict = {'int': int, 'str': str}
typlist = [typdict[t] for t in df['typ']]
dat = []


with open(datfile) as fin:
    for line in tqdm(fin):
        row = [line[start:stop] for (start, stop) in zip(df['start'], df['stop'])]
        dat.append([s.strip() for s in row])
        # dat.append(dict(typ(s.strip()) for typ, s in zip(typlist, [row for (i, row) in df.iterrows()])))

with open(datfile + '.csv', 'wt') as fout:
    fout.write(','.join(df['name']) + '\n')
    for line in tqdm(dat):
        fout.write(','.join(line) + '\n')


dat = pd.read_csv(datfile + '.csv', header=None)
dat.columns = df['name']
dat.to_csv(datfile + '.csv')
numerical_stats = dat.describe()
stats = dat.describe(include='all')
stats.to_csv(datfile + '.stats.csv')
stats[stats.loc['count'] > 0]
nonzerostats = stats[stats.columns.values[(stats.loc['count'] > 0).values]]
nonzerostats.to_csv(datfile + '.nonzerostats.csv')
df.to_csv('data/IAKR74FL/IAKR74FL.DAT.datadict.csv')
